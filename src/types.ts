import React from 'react';
import { ImageResizeMode } from 'react-native';

export interface IProps {
  images: ImageObject[];
}

export interface ImageObject {
  id?: string | number;
  thumbUrl?: string;
  url: string;
}
export interface ImagePreviewProps {
  item: ImageObject;
  resizeMode?: ImageResizeMode;
}
export interface RenderImageProps {
  item: ImageObject;
  index: number;
  resizeMode?: ImageResizeMode;
}
