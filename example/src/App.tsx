import React from 'react';
import { SafeAreaView, ScrollView, Text } from 'react-native';
import { ImageGallery } from '../../src/';
import { images } from './helpers';

const App = () => {
  return (
    <SafeAreaView>
      <ScrollView>
        <ImageGallery images={images} />

        <Text style={{ fontSize: 30, alignSelf: 'center', marginTop: 20 }}>
          Denim Jacket
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
